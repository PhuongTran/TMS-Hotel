
$(document).ready(function(){

	
	
	$('[data-toggle="tooltip"]').tooltip();
	$('nav#menu').mmenu();
	
	/*------- Select -------------*/
    $('.pt-select select').each(function(){
        var title = $(this).attr('title');
        if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .change(function(){
                val = $('option:selected',this).text();
                $(this).next().text(val);
            })
    });
	
	
	/*------- Datepicker -------------*/
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#dpd1').datepicker({
	  onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  if (ev.date.valueOf() > checkout.date.valueOf()) {
		var newDate = new Date(ev.date)
		newDate.setDate(newDate.getDate() + 1);
		checkout.setValue(newDate);
	  }
	  checkin.hide();
	  $('#dpd2')[0].focus();
	}).data('datepicker');
	var checkout = $('#dpd2').datepicker({
	  onRender: function(date) {
		return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  checkout.hide();
	}).data('datepicker');
	
	$('#dpd3').datepicker({
	  onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
	  }
	});
	/*------- Datepicker End -------------*/
	
	
	/* Swiper */
	var swiper = new Swiper('.swiper-container-01', {
        effect: 'fade',
		autoplay: {
			delay: 5000
		},pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
    });
	
	function  swiperperView (classSwiper,perView){
		var swiper = new Swiper(classSwiper, {
		  slidesPerView: perView,
		  spaceBetween: 30,
		  navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		  }
		});
		
	}
	
	if(window.matchMedia('(max-width: 766px)').matches) {
		swiperperView('.swiper-container-02',1);
		swiperperView('.swiper-icon-circle',1);
	}else{
		swiperperView('.swiper-container-02',3);
		swiperperView('.swiper-icon-circle',4);
	}
	
	
	$('.pt-report .icon-close').click(function(){
		$('.pt-report').hide()
	});
	
	
	$("[id-swiper=idswiper]").each(function(){
		var idSwiper = $(this)[0].id,
		idSwiper1 = "#"+idSwiper;
		swiperperView(idSwiper1,1);
	});
	
	
	
	/* Swiper End */
	
	
	$().fancybox({
	  selector : '[data-fancybox="filter"]:visible',
	  thumbs   : {
		autoStart : true
	  }
	});

	
});	
	